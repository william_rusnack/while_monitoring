# while_monitoring
Monitor DOM Elements for events to happen or not happen when a trigger function is called

Used to monitor if an event is or is not dispatched upon some trigger function

[![Build Status](https://travis-ci.org/BebeSparkelSparkel/while_monitoring.svg?branch=master)](https://travis-ci.org/BebeSparkelSparkel/while_monitoring)

## Uses
```javascript
await while_monitoring(element).expect(events).upon(cause [, timeout_ms])
```
Monitors element for the events to be dispatched when cause is called after timeout_ms.  
The promise rejects if events is not seen.  

```javascript
await while_monitoring(element).do_not_expect(events).upon(cause [, timeout_ms])
```
Monitors element for the events not to be dispatched when cause is called after timeout_ms.  
The promise rejects if events is seen.  

```javascript
await while_monitoring(element).expect(events).upon_event(causal_event [, timeout_ms])
```
Monitors element for the events to be dispatched when causal_event is dispatched on element after for timeout_ms.  
The promise rejects if events is not seen.  

```javascript
await while_monitoring(element).do_not_expect(events).upon_event(causal_event [, timeout_ms])
```  
Monitors element for the events not to be dispatched when causal_event is called after timeout_ms.  
The promise rejects if events is seen.  


## Input Definitions
**element** - HTML element to monitor.  
**events** - Event type or types to listen for. Example 'click' or ['click', 'change']  
**cause** - A function that should cause the event to dispatched on element.  
**causal_event** - String that specifies the event that will be dispatched on element.  
**timeout_ms** - Wait time before checking if event has happned. Defaults to 10ms.  


## Examples
Awaiting an event to happen upon a cause
```javascript
const input = document.querySelector('input')

async function() {

  await while_monitoring(input)
    .expect('change')
    .upon(() => {
      input.dispatchEvent(new Event('change'))
    })

}
```

## Install
```bash
npm install https://github.com/BebeSparkelSparkel/while_monitoring.git
```

### Browser
```html
<script src="/while_monitoring.js"></script>
```

### node
```javascript
const while_monitoring = require('while_monitoring')

/* Sets the event object
 * Not required if there is a global event object
 */
while_monitoring.Event = Event 
```
